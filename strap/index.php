<?php require_once('header.php'); ?>
<div class="container">
<div class="starter-template">
<br><br>
<!--<h1>Info</h1>
<p class="lead">This is my first task using Bootstrap.<br> I think Bootstrap has a good functionality to creaty fast and useful web pages.</p> -->
<?php
$sort = '';
$orderby = '';
$chpattern='';
	
if (!empty($_GET["d"])){
        $d = $_GET["d"];
        if ($d == 'asc'){
                $d = 'desc';
        }
        else
        {
                $d = 'asc';
        }
}
else
{
        $d = "asc";
}

if (!empty($_GET["sort"])){
        $sort = $_GET["sort"];
        $orderby = " order by ".$sort." ".$d;
    }
if (!empty($_GET["sfield"]) && !empty($_GET["spattern"])){
		$sfield = $_GET["sfield"];
		$spattern = mysqli_entities_fix_string($_GET["spattern"]);
        $chpattern = " where ".$sfield." like '%".$spattern."%' ";
    }
$num = 0;
?>
<div class="container">
<h2>Info about backups</h2>
  <p>Monitoring of the actually date of backups</p>
  <div>
  <form action="index.php">
  Select pattern:
  <input type="text" name="spattern" style="height: 25px; width: 140px;" <?php if(!empty($_GET["spattern"])){echo 'value="'.$_GET["spattern"].'"';}?>>
  <select name="sfield" style="height: 25px;">
  <option value="hostname" <?php if( isset($sfield) && $sfield == 'hostname' ){echo 'selected';}?> >hostname</option>
  <option value="container" <?php if( isset($sfield) && $sfield == 'container' ){echo 'selected';}?> >container</option>
  <option value="rserver" <?php if( isset($sfield) && $sfield == 'rserver' ){echo 'selected';}?> >backup server</option>
  <option value="backdate" <?php if( isset($sfield) && $sfield == 'backdate' ){echo 'selected';}?> >backdate</option>
  <option value="clversion" <?php if( isset($sfield) && $sfield == 'clversion' ){echo 'selected';}?> >client version</option>
  </select>
  <input type="submit" value="Submit">  
  </form>
  </div>
  <table class="table table-striped table-hover">
    <thead>
      <tr>
	<th>N</th>
        <th><a href="?sort=hostname&d=<?php echo $d;?>&sfield=<?php if(isset($sfield)){echo $sfield;} ?>&spattern=<?php if(isset($spattern)){echo $spattern;} ?>">Hostname</a></th>
        <th><a href="?sort=container&d=<?php echo $d;?>&sfield=<?php if(isset($sfield)){echo $sfield;} ?>&spattern=<?php if(isset($spattern)){echo $spattern;} ?>">Container</a></th>
	<th><a href="?sort=rserver&d=<?php echo $d;?>&sfield=<?php if(isset($sfield)){echo $sfield;} ?>&spattern=<?php if(isset($spattern)){echo $spattern;} ?>">Backup server</a></th>
        <th><a href="?sort=backdate&d=<?php echo $d;?>&sfield=<?php if(isset($sfield)){echo $sfield;} ?>&spattern=<?php if(isset($spattern)){echo $spattern;} ?>">Date</a></th>
	<th><a href="?sort=clversion&d=<?php echo $d;?>&sfield=<?php if(isset($sfield)){echo $sfield;} ?>&spattern=<?php if(isset($spattern)){echo $spattern;} ?>">Client version</a></th>
        <th>Remove</th>
      </tr>
    </thead>
    <tbody>

<?php
	$dbresult = mysqli_query($link, 'select * from backup_status'.$chpattern.$orderby);
	#print_r('select * from backup_status'.$chpattern.$orderby);

while($row = mysqli_fetch_array($dbresult,MYSQLI_ASSOC)){

//echo "hostname: ", $row[0], "<br>\n";
//echo "date: ", $row[1], "<br>\n";
//echo '<br>';
$num++;
//echo '<tr>';
if (substr($row['backdate'], 0, strpos($row['backdate'], ' ')) == date("Y-m-d")){
        echo '<tr class="active">';
 } else { 
	echo '<tr class="danger">'; }
echo '<td>'.$num.'</td>';
echo '<td>'.$row['hostname'].'</td>';
echo '<td>'.$row['container'].'</td>';
echo '<td>'.$row['rserver'].'</td>';
echo '<td>'.$row['backdate'].'</td>';
echo '<td>'.$row['clversion'].'</td>';
echo '<td><a href="index.php?deleteid='. $row['hostname'] .'" onclick="return confirm(\'Are you sure that you want to remove?\')">Remove info</a></td>';
echo '</tr>';

if (isset($_GET['deleteid'])) {
    $delid = $_GET['deleteid'];
    $deldrv = mysqli_query($link,"DELETE FROM backup_status WHERE `hostname` = '$delid' LIMIT 1")or die(mysqli_error());
    header("Location: index.php");
}

}

?>
<!--      <tr>
        <td>John</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>july@example.com</td>
      </tr> -->
    </tbody>
  </table>
  <div>Total: <?php print_r($num); ?></div><br>
</div>


</div>
</div><!-- /.container -->
<?php mysqli_close($link); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
</body></html>
