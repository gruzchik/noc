<?php require_once('header.php'); ?>
<div class="container">
<div class="starter-template">
<br><br>
<!--<h1>Info</h1>
<p class="lead">This is my first task using Bootstrap.<br> I think Bootstrap has a good functionality to creaty fast and useful web pages.</p> -->
<?php
$sort = '';
$orderby = '';
$chpattern='';

if (!empty($_GET["d"])){
	$d = $_GET["d"];
	if ($d == 'asc'){
		$d = 'desc';
	}
	else
	{
		$d = 'asc';
	}
}
else
{
	$d = "asc";
}

if (!empty($_GET["sort"])){
        $sort = $_GET["sort"];
        $orderby = " order by ".$sort." ".$d;
	}
if (!empty($_GET["sfield"]) && !empty($_GET["spattern"])){
		$sfield = $_GET["sfield"];
		$spattern = mysqli_entities_fix_string($_GET["spattern"]);
        $chpattern = " where ".$sfield." like '%".$spattern."%' ";
	}
$num = 0;
?>
<div class="container">
<h2>Checksum of the sites</h2>
  <p>Monitoring checksum of the files for sites</p>
  <p><a href="version.html">Version ChangeLog</a></p>
  <div>
  <form action="checksum.php">
  Select pattern:
  <input type="text" name="spattern" style="height: 25px; width: 140px;" <?php if(!empty($_GET["spattern"])){echo 'value="'.$_GET["spattern"].'"';}?>>
  <select name="sfield" style="height: 25px;">
  <option value="hostname" <?php if(isset($sfield) && $sfield == 'hostname' ){echo 'selected';}?> >hostname</option>
  <option value="checkdate" <?php if(isset($sfield) && $sfield == 'checkdate' ){echo 'selected';}?> >checkdate</option>
  <option value="status" <?php if(isset($sfield) && $sfield == 'status' ){echo 'selected';}?> >status</option>
  <option value="version" <?php if(isset($sfield) && $sfield == 'version' ){echo 'selected';}?> >version</option>
  </select>
  <input type="submit" value="Submit">  
  </form>
  </div>
  <table class="table table-hover">
    <thead>
      <tr>
	<th>N</th>
        <th><a href="?sort=hostname&d=<?php echo $d; ?>&sfield=<?php if(isset($sfield)){echo $sfield;} ?>&spattern=<?php if(isset($spattern)){echo $spattern;} ?>">Hostname</a></th>
        <th><a href="?sort=checkdate&d=<?php echo $d; ?>&sfield=<?php if(isset($sfield)){echo $sfield;} ?>&spattern=<?php if(isset($spattern)){echo $spattern;} ?>">Date of check</a></th>
        <th><a href="?sort=status&d=<?php echo $d; ?>&sfield=<?php if(isset($sfield)){echo $sfield;} ?>&spattern=<?php if(isset($spattern)){echo $spattern;} ?>">Status</a></th>
        <th><a href="?sort=version&d=<?php echo $d; ?>&sfield=<?php if(isset($sfield)){echo $sfield;} ?>&spattern=<?php if(isset($spattern)){echo $spattern;} ?>">Version</a></th>
	<th>Remove</th>
      </tr>
    </thead>
    <tbody>

<?php

$dbresult = mysqli_query($link,'select hostname, checkdate, status, version from checksum_status'.$chpattern.$orderby);

while($row = mysqli_fetch_array($dbresult,MYSQLI_ASSOC)){

//echo "hostname: ", $row[0], "<br>\n";
//echo "date: ", $row[1], "<br>\n";
//echo '<br>';
$num++;
if ($row['status'] == "ok"){
	echo '<tr class="success">';
} else {
echo '<tr class="danger">';
}
echo '<td>'.$num.'</td>';
echo '<td><b>'.$row['hostname'].'</b></td>';
echo '<td>'.$row['checkdate'].'</td>';
echo '<td>'.strtoupper($row['status']).'</td>';
echo '<td>'.$row['version'].'</td>';
echo '<td><a href="checksum.php?deleteid='. $row['hostname'] .'" onclick="return confirm(\'Are you sure that you want to remove?\')">Remove info</a></td>';
echo '</tr>';

if (isset($_GET['deleteid'])) {
    $delid = $_GET['deleteid'];
    $deldrv = mysqli_query($link,"DELETE FROM checksum_status WHERE `hostname` = '$delid' LIMIT 1")or die(mysqli_error());
    header("Location: checksum.php");
}

}

?>
<!--      <tr class="info">
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
      </tr> -->
    </tbody>
  </table>
  <div>Total: <?php print_r($num); ?></div><br>
</div>


</div>
</div><!-- /.container -->
<?php mysqli_close($link); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
</body></html>
