<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/starter-template/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Igor Pogorelko">
<link rel="icon" href="favicon.ico">
<title>Monitor v 1.0</title>
<!-- Bootstrap core CSS -->
<!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="examples/starter-template.css" rel="stylesheet">
</head>
<?php
ini_set('display_errors','On');
require_once('dbset.php');
function mysqli_fix_string($string)
{
	global $link;
	if (get_magic_quotes_gpc()){ $string = stripslashes($string); }
	return mysqli_real_escape_string($link, $string);
}
function mysqli_entities_fix_string($string)
{
	return htmlentities(mysqli_fix_string($string));
}
?>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
<div class="container">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand">Monitor</a>
</div>
<div id="navbar" class="collapse navbar-collapse">
<ul class="nav navbar-nav">
<?php
$arMenu = array(
            array(
                    "NAME" => "Backup",
                    "LINK" => "index.php",
            ),
            array(
                    "NAME" => "Checksum",
                    "LINK" => "checksum.php",
            ),
	    array(
                    "NAME" => "Mirroring",
                    "LINK" => "mirror.php",
            ),
            array(
                    "NAME" => "SSL",
                    "LINK" => "ssld.php",
            ),
);

foreach ($arMenu as $Item) {
            if ($Item["LINK"] == $_SERVER['PHP_SELF']) {
                    echo '<li class="active"><a href="'.$Item["LINK"].'">'.$Item["NAME"].'</a></li>';
            } else {
                    echo '<li><a href="'.$Item["LINK"].'">'.$Item["NAME"].'</a></li>';
            }
    }
?>
<!--
<li class="active"><a href="index.php">Backup</a></li>
<li><a href="checksum.php">Checksum</a></li>
<li><a href="first.html">Student</a></li>-->
</ul>
</div><!--/.nav-collapse -->
</div>
</nav>
