-- phpMyAdmin SQL Dump
-- version 4.4.15.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 28, 2016 at 04:50 PM
-- Server version: 10.0.24-MariaDB-1~wheezy
-- PHP Version: 5.4.45-0+deb7u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `holbiadmin_stats`
--

-- --------------------------------------------------------

--
-- Table structure for table `backup_status`
--

CREATE TABLE IF NOT EXISTS `backup_status` (
  `hostname` varchar(255) NOT NULL,
  `container` varchar(56) NOT NULL,
  `backdate` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checksum_status`
--

CREATE TABLE IF NOT EXISTS `checksum_status` (
  `id` int(11) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `checkdate` varchar(32) NOT NULL,
  `status` varchar(32) NOT NULL,
  `version` varchar(32) NOT NULL,
  `report` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mirror_status`
--

CREATE TABLE IF NOT EXISTS `mirror_status` (
  `hostname` varchar(255) NOT NULL,
  `backdate` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backup_status`
--
ALTER TABLE `backup_status`
  ADD PRIMARY KEY (`hostname`);

--
-- Indexes for table `checksum_status`
--
ALTER TABLE `checksum_status`
  ADD UNIQUE KEY `hostname` (`hostname`);

--
-- Indexes for table `mirror_status`
--
ALTER TABLE `mirror_status`
  ADD PRIMARY KEY (`hostname`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
