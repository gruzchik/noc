-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 09 2015 г., 22:45
-- Версия сервера: 10.0.22-MariaDB
-- Версия PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `u744321861_monit`
--

-- --------------------------------------------------------

--
-- Структура таблицы `backup_status`
--

DROP TABLE IF EXISTS `backup_status`;
CREATE TABLE IF NOT EXISTS `backup_status` (
  `hostname` varchar(255) NOT NULL,
  `backdate` varchar(32) NOT NULL,
  PRIMARY KEY (`hostname`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `backup_status`
--

INSERT INTO `backup_status` (`hostname`, `backdate`) VALUES
('bandplace.com', '2015-12-09 02:00:05'),
('cheap.co.uk', '2015-12-09 02:02:05'),
('dstory11.co.uk', '2015-12-09 02:50:27'),
('dstory12.co.uk', '2015-12-09 01:09:57'),
('dstory20d.co.uk', '2015-12-09 02:00:17'),
('mstory1.co.uk', '2015-12-09 02:00:14'),
('mstory7.co.uk', '2015-12-09 01:59:44'),
('mstory8.co.uk', '2015-12-09 01:57:52'),
('beatt.co.uk', '2015-12-09 02:00:12'),
('server.story.com', '2015-12-09 16:57:35'),
('serveremo1.servere.com', '2015-12-09 09:02:28'),
('websites.co.uk', '2015-12-09 02:00:22'),
('website.no', '2015-12-09 02:00:11'),
('story.org', '2015-12-09 12:04:35');

-- --------------------------------------------------------

--
-- Структура таблицы `checksum_status`
--

DROP TABLE IF EXISTS `checksum_status`;
CREATE TABLE IF NOT EXISTS `checksum_status` (
  `id` int(11) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `checkdate` varchar(32) NOT NULL,
  `status` varchar(32) NOT NULL,
  `version` varchar(32) NOT NULL,
  `report` text NOT NULL,
  UNIQUE KEY `hostname` (`hostname`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `checksum_status`
--

INSERT INTO `checksum_status` (`id`, `hostname`, `checkdate`, `status`, `version`, `report`) VALUES
(0, 'cheely.com', '2015-08-13 16:52:16', 'ok', '0.4.150417', ''),
(0, 'deash.co.uk', '2015-12-04 13:14:57', 'ok', '0.4.150417', ''),
(0, 'daddy.co.uk', '2015-09-14 16:25:14', 'ok', '0.4.150417', ''),
(0, 'elfcountry.co.uk', '2015-11-25 10:06:57', 'ok', '0.4.150417', ''),
(0, 'folk.world.org', '2015-06-22 11:29:53', 'ok', '0.4.150417', ''),
(0, 'funnyworld.com', '2015-12-04 17:55:41', 'ok', '0.4.150417', ''),
(0, 'funnycountry.co.uk', '2015-08-13 14:51:21', 'ok', '0.4.150417', ''),
(0, 'grace.eu', '2015-12-09 10:28:10', 'ok', '0.4.150417', ''),
(0, 'i-ron.com', '2015-10-20 10:47:17', 'ok', '0.4.150417', ''),
(0, 'ione.im', '2015-07-07 11:15:34', 'ok', '0.4.150417', ''),
(0, 'smartfox.co.uk', '2015-12-04 17:56:22', 'ok', '0.4.150417', ''),
(0, 'kindforest.co.uk', '2015-11-25 14:34:14', 'ok', '0.4.150417', ''),
(0, 'lunalight.co.uk', '2015-12-04 13:12:57', 'ok', '0.4.150417', ''),
(0, 'nouone.com', '2015-04-22 17:45:54', 'ok', '0.4.150417', ''),
(0, 'package.co.uk', '2015-05-13 11:35:20', 'ok', '0.4.150417', ''),
(0, 'forme-store.co.uk', '2015-09-14 16:15:44', 'ok', '0.4.150417', ''),
(0, 'phsign.co.uk', '2015-11-25 10:08:29', 'ok', '0.4.150417', ''),
(0, 'portandable.co.uk', '2015-11-25 11:33:22', 'ok', '0.4.150818', ''),
(0, 'roaddirect.co.uk', '2015-09-14 16:34:51', 'ok', '0.4.150417', ''),
(0, 'sart.com', '2015-09-14 15:43:43', 'ok', '0.4.150417', ''),
(0, 'goodweek.com', '2015-12-04 17:57:32', 'ok', '0.4.150417', ''),
(0, 'thefineshop.co.uk', '2015-08-11 10:55:10', 'ok', '0.4.150417', ''),
(0, 'teddy-shop.co.uk', '2015-10-12 17:34:47', 'ok', '0.4.150417', '');

-- --------------------------------------------------------

--
-- Структура таблицы `mirror_status`
--

DROP TABLE IF EXISTS `mirror_status`;
CREATE TABLE IF NOT EXISTS `mirror_status` (
  `hostname` varchar(255) NOT NULL,
  `backdate` varchar(32) NOT NULL,
  PRIMARY KEY (`hostname`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `mirror_status`
--

INSERT INTO `mirror_status` (`hostname`, `backdate`) VALUES
('mstory5.co.uk', '2015-12-09 09:57:55'),
('server.monitor.com', '2015-12-09 01:01:33'),
('towels.co.uk', '2015-12-09 01:02:03');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;